import { HomeView } from 'src/sections/home/view';

// ----------------------------------------------------------------------

export const metadata = {
  title: 'BGSS',
};
export default function HomePage() {
  return <HomeView />;
}
